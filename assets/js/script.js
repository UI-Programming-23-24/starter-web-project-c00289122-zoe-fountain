//js
// below I declare an array of different colours
let colours = ["green", "blue", "red", "orange", "pink"]
let size = ["100x100", "200x200", "300x300", "400x400", "500x500"]

//lists the event 'click' to allow the box to be clicked
let box = document.getElementById("box");
box.addEventListener ("click", changeBoxColour)
box.addEventListener ("click", changeBoxSize)
help.addEventListener("mouseover", surprise);
help.addEventListener("mouseleave", bye);

//function that selects a new colour

function changeBoxColour(){
   let newColour = colours[Math.floor(Math.random() * colours.length)];
   console.log("new box colour is:")
   console.log(newColour);
   box.style.backgroundColor = newColour;
}

function changeBoxSize(){
   let newSize = Math.random();
   console.log("new box size is: ")
   console.log(newSize);
   box.style.scale = newSize;
}

function surprise()
{
   let newColour = "gray";
   console.log("new colour is:")
   console.log(newColour);
   help.style.backgroundColor = newColour;
}
function bye()
{
   let newColour = "hotpink";
   console.log("new colour is:")
   console.log(newColour);
   help.style.backgroundColor = newColour;
}